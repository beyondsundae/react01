import React, {Component} from 'react';
class LoginForm extends Component{
    state = {
        formElement:{
            username:{
                type:'text',
                value:'',
                validator:{
                    required:true,
                    minLength:5,
                    maxLength:10
                },
            touched:false,
            error:{status:true, message:''}
            },

            password:{
                type:'password',
                value:'',
                validator:{
                    required:true,
                    minLength:6
                    
                },
            touched:false,
            error:{status:true, message:''}
            }
        },
        formValid:false
    }

    onInputChange = (event)=>{
        this.setState({
            [event.target.name]: event.target.value
            
        })

    }

    onLoginSubmit = (event)=>{
        event.preventDefault();
        console.log(this.state);
    }

    /*checkValidator=(value,rule)=>{
        let valid = true;
        let message = '';
        if(value.trim().length===0 && rule.required){
            valid = false;
            message = 'fill this i sus'
        }

        if(value.length < rule.minLength && valid){
            valid = false;
            message ="this input less than ${rule.minLength}";
        }

        if(value.length > rule.minLength && valid){
            valid = false;
            message ="this input more than ${rule.maxLength}";
        }
        return{status:!valid, message:message};
    }

    onFormChange =(event)=>{
        const name = event.target.name;
        const value = event.target.value;
        let updatedForm = {...this.state.formElement};
        updatedForm[name].value = value;
        updatedForm[name].touched= true;
        const validatorObject = this.checkValidator(value, updatedForm[name].validator);
        updatedForm[name].error={
            status:validatorObject.status,
            message:validatorObject.message
        }
        let formStatus =true;
        for (let name in updatedForm){
            if(updatedForm[name].validator.required===true){
                formStatus=!updatedForm[name].error.status && formStatus;
            }
        }
    
    this.setState({
        ...this.stste,
        formElement:updatedForm,
        formValid:formStatus
    });

    /*getInputClass =(name)=>{
        const elementErrorStatus = this.state.formElement[name].error.status;
        return elementErrorStatus && this.state.formElement[name].touch?
        'form-control is-invalid':
        'form-control is-valid';
    }*/




    render(){
        return(
            <div className='container'>
            <div className='col mt-5 mx-auto'>
                <div className='card card-body shadow' >
                    <form onSubmit={this.onLoginSubmit}>
                        <div className='form-group'>
                            <label>Username</label>
                            <input 
                                type="text"
                                className='form-control'
                                id='username'
                                name='username'
                                onChange={this.onInputChange}/>
                            <div className='valid-feedback'>xxxx</div>
                        </div>

                        <div className='form-group'>
                            <label>Password</label>
                            <input 
                                type="text"
                                className='form-control'
                                id='password'
                                name='password'
                                onChange={this.onInputChange}/>
                        </div>

                        <div className='text-center'>
                            <button 
                                type='submit'
                                className='btn btn-success my-1'>Log in
                            </button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        )
    }

}

export default LoginForm;