import React from 'react';
import PostItem from './Post-item';
const Post = (props) =>{
    const id = props.match.params.id;

    let post = null;
    if(id === '1')
        {
            post = (
                <PostItem title='page1' content='content yedped 1'/>
            );
        }

    else if(id ==='2')
    {
            post = (
                <PostItem title='page2' content='content yedped 2'/>
            );
    }
    return(
        <div>
            {post}
        </div>
    );
}

export default Post;