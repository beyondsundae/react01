import React, {Component} from 'react';
import'bootstrap/dist/css/bootstrap.min.css';

class Child extends Component{ 
    onCouponUse = (event)=>{
        this.props.setCoupon();
    }
    resetCoupon = (event)=>{
        this.props.resetCoupon();
    }


render(){
    const status = this.props.data.status;
    const coupon = this.props.data.secretWord;

    return(
        <div className='col col-sm-6 mt-3 mx-auto'>
            <div className='card mx-auto'>
                <div className='card-header text-center'>
                    <button className='btn btn-success mx-5' disbled={!status}
                    onClick={this.onCouponUse}>
                        Use Coupon
                    </button>
                    
                    <button className='btn btn-success mx-5' disbled={!status}
                    onClick={this.resetCoupon}>
                        Reset
                    </button>
                </div>
                
                <div className='card-body'>
                    <span className='mr-2'>Coupon:</span>
                    <span className='badge badge-info'>
                        {status?coupon:'This coupon already used'}
                    </span>
                    
                </div>

            </div>
        </div>
    )  
}
}
export default Child;
