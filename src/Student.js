import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const Student =(props)=>{
    const {data, editStudent} = props;

    const updatedNameInput = React.createRef();

            const ClickToDeleteStudent =()=>{
                props.deleteStudent();
                }

            const ClickToEditStudent =()=>{
                const editData ={
                    id: data.id,
                    name: data.name,
                    editingStatus: !data.editingStatus
                };
                editStudent(data.id, editData);
                console.log(data.id, editData);
                }

                    const ClickToConfirmEdit =()=>{
                        const editData={
                            id: data.id,
                            name: updatedNameInput.current.value,
                            editingStatus: false
                        };
                        editStudent(data.id, editData);
                        }
                    
                    const ClickToCancelEdit =()=>{
                        const editData={
                            id: data.id,
                            name: data.name,
                            edtingStatus: false};
                            editStudent(data.id, editData);
                        }


        const editForm = (
            
            <div className='row'>
                <div className='input-group mb-3'>
                    <input
                        type='text'
                        name='updatedName'
                        className='form-control col-6'
                        //defaultValue={data.name}
                        ref={updatedNameInput}
                        
                        
                    />
                    
                    <button
                        onClick={ClickToCancelEdit.bind()} 
                        className='btn btn-primary btn-sm ml=1 col-3'>
                        Cancel
                    </button>

                    <button
                        onClick={ClickToConfirmEdit.bind()} 
                        className='btn btn-primary btn-sm ml=1 col-3'>
                        Ok
                    </button>
                </div>
            </div>
        )


return(
    
    <div className='card'>
        <div className='card-header text-center'>
            <button onClick={ClickToEditStudent} className='btn btn-success mr-1'>Edit</button>
            <button onClick={ClickToDeleteStudent} className='btn btn-danger ml-1'>Delete</button>
        </div>

        <div className="card-body">
            <dl className='row'>
                <dt className='col-6'>id:</dt>
                <dt className='col-6'>{props.data.id}</dt>
                <dt className='col-6'>name:</dt>
                <dt className='col-6'>{props.data.name}</dt>

            </dl>

            {
                props.data.editingStatus?
                editForm:
                null
            }
        </div>
    </div>
)

}
export default Student;